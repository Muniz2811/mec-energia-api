import pytest
from users.models import CustomUser, UniversityUser
from utils.user.user_type_util import UserType

def test_CT1():
    user_type = 'university_tester'
    esperado = f'User type (university_tester) does not exist'
    with pytest.raises(Exception) as context:
        UserType.is_valid_user_type(user_type, user_model=UniversityUser)
    
    actual_message = str(context.value)

    assert actual_message == esperado, f"Esperado: {esperado}, Obtido: {actual_message}"



def test_CT2():
    user_type = 'super_user'
    esperado = f"Wrong User type (super_user) for this Model User (<class 'users.models.UniversityUser'>)"
    with pytest.raises(Exception) as context:
        UserType.is_valid_user_type(user_type, user_model=UniversityUser)
    
    actual_message = str(context.value)

    assert actual_message == esperado, f"Esperado: {esperado}, Obtido: {actual_message}"


def test_CT3():
    user_type = 'university_user'
    try: 
        saida = UserType.is_valid_user_type(user_type, user_model=UniversityUser)
        assert saida == user_type
    except Exception as e:
        pytest.fail(f"erro inesperado: {e}")

def test_CT4():
    user_type = 'university_user'
    esperado = f"Wrong User type (university_user) for this Model User (<class 'users.models.CustomUser'>)"
    with pytest.raises(Exception) as context:
        UserType.is_valid_user_type(user_type, user_model=CustomUser)
    
    actual_message = str(context.value)

    assert actual_message == esperado, f"Esperado: {esperado}, Obtido: {actual_message}"

def test_CT5():
    user_type = 'super_user'
    try: 
        saida = UserType.is_valid_user_type(user_type, user_model=CustomUser)
        assert saida == user_type
    except Exception as e:
        pytest.fail(f"erro inesperado: {e}")
