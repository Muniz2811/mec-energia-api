import pytest 
from utils.cnpj_validator_util import CnpjValidator


def test_TDD1():
    cnpj = "12345678901234567890"
    esperado = "12.345.678/9012-34"
    try: 
        saida = CnpjValidator.format_cnpj(cnpj)
        assert saida == esperado
    except Exception as e:
        pytest.fail(f"erro inesperado: {e}")


def test_TDD2():
    cnpj2 = "12345678901234"
    esperado = "12.345.678/9012-34"
    try: 
        saida = CnpjValidator.format_cnpj(cnpj2)
        assert saida == esperado
    except Exception as e:
        pytest.fail(f"erro inesperado: {e}")



def test_TDD3():
    cnpj3 = '12345'
    esperado = f'Cnpj deve ter 14 digitos para que possa ser formatado'
    with pytest.raises(Exception) as context:
        CnpjValidator.format_cnpj(cnpj3)
    
    actual_message = str(context.value)

    assert actual_message == esperado, f"Esperado: {esperado}, Obtido: {actual_message}"